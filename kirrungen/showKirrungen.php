<?php

# showKirrungen.php: Darstellung von Basisstationen und Clients
# Dargestellte Informationen:
#   ID
#   Nutzer
#   Name des Gerätes
#   Standort des Gerätes
#   Batteriespannung
#   Empfangsqualität (Lora und GSM)
#   Letzte Aktivität

include $_SERVER['DOCUMENT_ROOT'] . "/checkPermission.php";
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";?>
    <!DOCTYPE html>
    <html>

    <body>
        <article>
            <h1>Aktuelle Meldungen der Kirrungsmelder: </h1>
            <form action="https://www.kleber.dynu.net/kirrungen/index.php?show=list" method="post">
                <input type="submit" value="Liste anzeigen">
            </form>
            <?php

# Auslesen der Anzahl an Basisstationen und deren IDs:
$i = 0;
$statement = $pdo->prepare("SELECT * FROM basis WHERE user = ? ORDER BY last_message DESC");
$statement->execute(array($_SESSION['userid']));
while ($row = $statement->fetch()) {
    $device[$i] = $row['device_id'];
    $i = $i + 1;
}
# Auslesen des Nutzer-Namens:
$statement3 = $pdo->prepare("SELECT * FROM users WHERE id = ?");
$statement3->execute(array($_SESSION['userid']));
while ($row3 = $statement3->fetch()) {
    $vorname = $row3['vorname'];
}
?>
                <table border="1" width="900" cellspacing="0">
                    <caption>Liste der Basisstationen:</caption>
                    <?php
# Auslesen der Basisstation-Kenndaten und Erstellung einer HTML-Tabelle:
$statement = $pdo->prepare("SELECT * FROM basis WHERE user = ? ORDER BY last_message DESC");
$statement->execute(array($_SESSION['userid']));?>
                        <thead>
                            <tr>
                                <th>ID</td>
                                    <th>Nutzer</td>
                                        <th>Name</td>
                                            <th>Standort</td>
                                                <th>Batterie</td>
                                                    <th>GSM QOS</td>
                                                        <th>Letzte Aktivität</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
# Befüllung der Tabelle mit den Basiskenndaten:
while ($row = $statement->fetch()) {
    $device[$i] = $row['device_id'];
    echo "<tr>";
    echo "<td>" . $row['device_id'] . "</td>";
    echo "<td>" . $vorname . "</td>";
    echo "<td>" . $row['name'] . "</td>";
    echo "<td>" . $row['location'] . "</td>";
    echo "<td>" . $row['battery_voltage'] . "</td>";
    echo "<td>" . $row['gsm_qos'] . "</td>";
    echo "<td>" . date('d.m.y H:i:s', strtotime($row['last_message'])) . "</td>";

    echo "</tr>";
}
echo "</tbody>";
echo "</table>";
echo "<br /> <br /> ";

# Erstellung der HTML-Tabelle für die Kirrungsmelder (Client):
?>
                                <table border="1" width="900" cellspacing="0">
                                    <caption>Liste der Kirrungsmelder:</caption>
                                    <thead>
                                        <tr>
                                            <th>ID</td>
                                                <th>Zug. Basis</td>
                                                    <th>Name</td>
                                                        <th>Standort</td>
                                                            <th>Batterie</td>
                                                                <th>Lora QOS</td>
                                                                    <th>Letzte Aktivität</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

# Befüllung der Tabelle mit Client-Daten:
for ($k = 0; $k < $i; $k++) {
    $statement2 = $pdo->prepare("SELECT * FROM client WHERE device_basis = ? ORDER BY last_alarm DESC");
    $statement2->execute(array($device[$k]));

    while ($row2 = $statement2->fetch()) {
        echo "<tr>";
        echo "<td>" . $row2['device_id'] . "</td>";
        echo "<td>" . $row2['device_basis'] . "</td>";
        echo "<td>" . $row2['name'] . "</td>";
        echo "<td>" . $row2['location'] . "</td>";
        echo "<td>" . $row2['battery_voltage'] . "</td>";
        echo "<td>" . $row2['lora_qos'] . "</td>";
        echo "<td>" . date('d.m.y H:i:s', strtotime($row2['last_alarm'])) . "</td>";

        echo "</tr>";

    }
}
echo "</tbody>";
echo "</table>";

echo "<br /> <br /> ";

?>
        </article>
        </article>
    </body>

    </html>