 <?php

# Index.php des Unterordner Kirrungen
# Aufbau analog zur Haupt.Index.php
# Zus�tzlich kann �ber GET-Parameter 'show' zwischen der Gesamtanzeige und einer Kirrungs-liste gewechselt werden
# Aufruf der Inhaltsdateien
#   showKirrungen.php
#   kirrlist.php

include $_SERVER['DOCUMENT_ROOT'] . "/head.php";
$check = 'show';
$modus = $_GET[$check];

if (strcmp($modus, "list") == 0) {
    include "kirrlist.php";
} else {
    include "showKirrungen.php";
}

include $_SERVER['DOCUMENT_ROOT'] . "/bottom.php";
?>

