<?php

# kirrlist.php: Anzeige einer Liste von allen letzten Alarmen (Datenbanktabelle "alarm")
# Es werden maximal 10 Alarme pro Kirrungsmelder angezeigt
# Die Liste kann über einen Button gelöscht werden
# Über einen zweiten Button kann zwischen Listenansicht und Gesamtanzeige gewechselt werden

include $_SERVER['DOCUMENT_ROOT'] . "/checkPermission.php";
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";

?>
    <!DOCTYPE html>
    <html>

    <body>
        <article>
            <h1>Die letzten Meldungen der Kirrungsmelder: </h1>
            <form action="?show=all" method="post">
                <input type="submit" value="Alles anzeigen">
            </form>
            <?php

# Wenn Post-Parameter "Delete" gesetzt ist, werden alle Einträge des Nutzers in der Tabelle "alarm" gelöscht
$i = 0;
if (isset($_POST['delete'])) {
    $statement = $pdo->prepare("DELETE FROM alarm WHERE user = ?");
    $statement->execute(array($_SESSION['userid']));
    $delete = $statement->fetchAll();

}


# Auslesen der Tabelleneinträge und Ausgabe in einer HTML-Tabelle:
$statement = $pdo->prepare("SELECT * FROM alarm WHERE user = ? ORDER BY alarm_time DESC");
$statement->execute(array($_SESSION['userid']));

?>
                <table border="1" width="900" cellspacing="0">
                    <caption>Liste der Kirrungsmelder:</caption>
                    <thead>
                        <tr>
                            <th>Name</td>
                                <th>ID</td>
                                    <th>Alarm Zeit</td>
                                        <th>Info</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
    # Überprüfung, dass nur die 10 letzten Einträge pro Client ausgelesen werden:
$prevClient = "";
while ($row = $statement->fetch()) {
    $i = $i + 1;

    if (strcmp($row['client'], $prevClient) != 0) {
        $i = 0;
        $prevClient = $row['client'];
        $statement2 = $pdo->prepare("SELECT name FROM client WHERE device_id = ?");
        $statement2->execute(array($row['client']));
        $name = $statement2->fetch();
    }
    if ($i < 10) {
        # Ausgabe von Name, ID, Alarmzeit und Alarmcode als Information
        echo "<tr>";
        echo "<td>" . $name['name'] . "</td>";
        echo "<td>" . $row['client'] . "</td>";
        echo "<td>" . date('d.m.y H:i:s', strtotime($row['alarm_time'])) . "</td>";
        switch ($row['alarmcode']) {
            case 51:echo "<td> Anmeldung Gerät </td>";
                break;
            case 34:
                echo "<td> Alarm </td>";
                break;
            default:echo "<td> Unbekannter Alarmcode </td>";
        }

        echo "</tr>";
    }
}

echo "</tbody>";
echo "</table>";

echo "<br /> <br /> ";

# Darstellung des Lösch-Buttons:
?>
                            <center>
                                <form action="?show=list" method="post">
                                    <input type="submit" name="delete" value="Liste leeren">
                                </form>
                            </center>
        </article>
        </article>
    </body>

    </html>