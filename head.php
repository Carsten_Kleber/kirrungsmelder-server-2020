<?php
# Hauptseite, Erstellen des Menübandes und des Designs in Abhängigkeit des CSS-Style-Sheets
# Datei enthält nur den oberen Teil der Website
# Verwendung immer mit bottom.php und einer Inhaltsseite

$reg_enable = true; # Aktivierung und Deaktivierung der Registrierung
?>
    <html>

    <head>
        <title> Kirrserver </title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="https://www.kleber.dynu.net/style.css">
        <!-- Einbinden des Style-Sheets -->
    </head>

    <body>
        <div id="wrapper" class="shadow">
            <header>
            </header>
            <nav>
                <ul>
                    <!-- Anzeige der URL-Links im Menüband -->
                    <li><a href=https://www.kleber.dynu.net>Startseite</a></li>
                    <li><a href=https://www.kleber.dynu.net/kirrungen>Kirrungen</a></li>
                    <li><a href=https://www.kleber.dynu.net/geraete>Geräte</a></li>
                    <li><a href=https://www.kleber.dynu.net/aktivierung>Aktivierung</a></li>
                    <li><a href=https://www.kleber.dynu.net/telegram>Alarmierung</a></li>

                    <?php
// Je nach Login-Status des Nutzers werden unterschiedliche Menüpunkte angezeigt
// Angemeldet: nur Logout, Sonst: Login und Registrierung
Session_Start();

if (!isset($_SESSION['userid'])) {
    echo "<li><a href=https://www.kleber.dynu.net/login>Login</a></li>";
    echo "<li><a href=https://www.kleber.dynu.net/reg>Registrieren</a></li>";
} else {
    echo "<li><a href=https://www.kleber.dynu.net/logout>Logout</a></li>";
}

?>
                </ul>
            </nav>
            <main>
