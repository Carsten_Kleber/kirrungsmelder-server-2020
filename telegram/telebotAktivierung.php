<?php

# telebotAktivierung.php: Regelt die Einstellungen zur Alarmierung auf der Website
# �ber die variablen telegram_active und mail_Active werden die Benachrichtigungen in der
# Datenbank "users" gespeichert

include $_SERVER['DOCUMENT_ROOT'] . "/checkPermission.php";
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";

?>
    <article>
        <h1> Aktivierung einer Basisstation: </h1>
        <!DOCTYPE html>
        <html>

        <head>
        </head>

        <body>
            <?php
# Auslesen der aktuellen Einstellungen:
$statement = $pdo->prepare("SELECT telegram_active, mail_active FROM users WHERE id = :id");
$result = $statement->execute(array('id' => $_SESSION['userid']));
$status = $statement->fetch();

# �nderung der Einstellungen nach Abschicken des Formulars
if (isset($_GET['change'])) {
    $telegram_active = $_POST['telegram'];
    $mail_active = $_POST['mail'];

    if ($telegram_active) {$enable_tel = 1;} else { $enable_tel = 0;}
    if ($mail_active) {$enable_mail = 1;} else { $enable_mail = 0;}

    $statement = $pdo->prepare("UPDATE users SET telegram_active = :telegram_active, mail_active = :mail_active WHERE id = :id");
    $result = $statement->execute(array('telegram_active' => $enable_tel, 'mail_active' => $enable_mail, 'id' => $_SESSION['userid']));
    $user = $statement->fetch();

    echo "Ihre Wahl wurde gespeichert!";
    echo "<li><a href=https://www.kleber.dynu.net>Startseite</a></li>";
    echo " <li><a href=https://www.kleber.dynu.net/telegram>Zurueck</a></li>";

} else {
    ?>
                <!-- Darstellung der Check-Boxen als HTML-Formular -->
                <form action="?change=1" method="post">
                    <h3>Kreuzen Sie an ob Telegram und/oder die Benachrichtung per Mail aktiviert sein sollen!:</h3>
                    <fieldset>
                        <ul>
                            <label>
          <input type="checkbox" name="telegram" value="telegram"
<?php

    if ($status['telegram_active']) {
        echo "checked";
    }?>
    >
          Telegram
        </label>
                            <label>
          <input type="checkbox" name="mail" value="mail"
<?php
if ($status['mail_active']) {
        echo "checked";
    }?>
    >
          Email
        </label>
                        </ul>
                    </fieldset>
                    <input type="submit" value="Abschicken"><br><br>
                </form>
                <?php
}
?>
        </body>

        </html>
    </article>
