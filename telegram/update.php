<?php

# update.php: Datei zur Berarbeitung der Telegram-Nachrichten
#   Unterscheidung zwischen:
#       Text-Nachricht
#       Standort-Nachricht
#       Callback-Query --> Antwort auf Inline-Button
#       Inline-Query (wird nicht verwendet)
# Mehr Informationen unter: https://core.telegram.org/bots/api

# �berpr�fung der Nachricht auf Callback-Query:
# --> Funktion: R�ckgabe von Clients oder Basisstationen
$callback_query = $telegram->Callback_Query();
if ($callback_query !== null && $callback_query != '') {

    if ($telegram->Callback_Data() == '1') {
        $reply = 'Ihre Wahl: Client';
    } else {
        $reply = 'Ihre Wahl: Basis';
    }
    # Wahl wird best�tigt
    $content = ['callback_query_id' => $telegram->Callback_ID(), 'text' => $reply, 'show_alert' => false];
    $telegram->answerCallbackQuery($content);

    #Ausgabe Kirrungsliste Basis oder Client, Aufbau �hnlich wie Website Kirrungen
    if ($telegram->Callback_Data() == '1') {
        $statement = $pdo->prepare("SELECT device_id FROM basis WHERE user = ? ORDER BY last_message DESC");
        $statement->execute(array($id));
        $count = 0;
        while ($result = $statement->fetch()) {$device[$count] = $result['device_id'];
            $count++;}
        $client = "Kirrungsmelder:";
        for ($k = 0; $k < $count; $k++) {
            $statement = $pdo->prepare("SELECT name, last_alarm FROM client WHERE device_basis = ? ORDER BY last_alarm DESC");
            $statement->execute(array($device[$k]));
            while ($result = $statement->fetch()) {
                $client = $client . "\n" . $result['name'] . " - " . date('d.m.y H:i:s', strtotime($result['last_alarm']));
            }
        }
        $content = ['chat_id' => $telegram->Callback_ChatID(), 'text' => $client];
        $telegram->sendMessage($content);
        $callback_query = null;
    } else {

        $statement = $pdo->prepare("SELECT * FROM basis WHERE user = ? ORDER BY last_message DESC");
        $statement->execute(array($id));
        $basis = "Basisstationen:";

        while ($result = $statement->fetch()) {
            $basis = $basis . "\n" . $result['name'] . " - " . date('d.m.y H:i:s', strtotime($result['last_message']));

        }
        $content = ['chat_id' => $telegram->Callback_ChatID(), 'text' => $basis];
        $telegram->sendMessage($content);
        $callback_query = null;
    }
}

# �berpr�fung, ob Inline Query: Nicht verwendet
if ($data['inline_query'] !== null && $data['inline_query'] != '') {
    $query = $data['inline_query']['query'];
    $location = $data['inline_query']['location'];

    // GIF Examples
    /* if (strpos('testText', $query) !== false) {
$results = json_encode([['type' => 'gif', 'id' => '1', 'gif_url' => 'http://i1260.photobucket.com/albums/ii571/LMFAOSPEAKS/LMFAO/113481459.gif', 'thumb_url' => 'http://i1260.photobucket.com/albums/ii571/LMFAOSPEAKS/LMFAO/113481459.gif']]);
$content = ['inline_query_id' => $data['inline_query']['id'], 'results' => $results];
$reply = $telegram->answerInlineQuery($content);

}
if (strpos('hochsitz', $query) !== false) {
$results = json_encode([['type' => 'location', 'id' => '2', 'latitude' => $location['latitude'], 'longitude' => $location["longitude"], 'title' => 'Hochsitz']]);
$content = ['inline_query_id' => $data['inline_query']['id'], 'results' => $results];
$reply = $telegram->answerInlineQuery($content);

}

if (strpos('dance', $query) !== false) {
$results = json_encode([['type' => 'gif', 'id' => '1', 'gif_url' => 'https://media.tenor.co/images/cbbfdd7ff679e2ae442024b5cfed229c/tenor.gif', 'thumb_url' => 'https://media.tenor.co/images/cbbfdd7ff679e2ae442024b5cfed229c/tenor.gif']]);
$content = ['inline_query_id' => $data['inline_query']['id'], 'results' => $results];
$reply = $telegram->answerInlineQuery($content);
}*/
}

# �berpr�fung, ob Nachricht eine Text-Nachricht ist:
#   --> �berpr�fung von allen Befehle und R�cksendung einer entsprechenden Antwort
if (!is_null($text) && !is_null($chat_id)) {
    if ($text == '/start') {
        $reply = 'Willkommen auf dem Kirrserver. Mit /help koennen Sie meine Befehle einsehen.';
        // Create option for the custom keyboard. Array of array string
        $option = [['Okay!'], ['/help']];
        // Get the keyboard
        $keyb = $telegram->buildKeyBoard($option, $onetime = true, $resize = true);
        $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => $reply];
        $telegram->sendMessage($content);
    } elseif ($text == '/web') {
        $reply = 'Weitere Informationen auf der Website: https://www.kleber.dynu.net';
        // Build the reply array
        $content = ['chat_id' => $chat_id, 'text' => $reply];
        $telegram->sendMessage($content);
    } elseif ($text == '/hallo') {
        $message = 'Hallo ' . $name;
        $content = array('chat_id' => $chat_id, 'text' => $message);
        $telegram->sendMessage($content);

    } else if (strcmp($text, "/from") == 0) {
        $message = sprintf("Group %d", $group);
        $content = array('chat_id' => $chat_id, 'text' => $message);
        $telegram->sendMessage($content);
    } else if (strcmp($text, "/help") == 0) {
        $message = "Der Kirr-Bot kann folgende Befehle: \n /start - Information \n /reg=email - Registrierung (Email ersetzen) \n /kirrungen - Liste Kirrungsmelder \n /last - Letzer Alarm \n /location?client - Standort abfragen \n /web - Link zur Website \n /setLocation=client - Standort setzen \n /unreg - Kirrbot abmelden";
        $content = array('chat_id' => $chat_id, 'text' => $message);
        $telegram->sendMessage($content);
    } elseif (strncmp($text, "/location", 8) == 0) {
        // Build the reply array
        $client = strtok($text, '?');
        $client = strtok('?');
        $statement = $pdo->prepare("SELECT * FROM client WHERE name = :name");
        $result = $statement->execute(array('name' => $client));
        $user = $statement->fetch();
        if ($user['location']) {
            $long = strtok($user['location'], ' ');
            $lat = strtok(' ');
            $content = array('chat_id' => $chat_id, 'longitude' => $long, 'latitude' => $lat);
            $telegram->sendLocation($content);
        }
    } elseif ($text == '/last') {
        $statement = $pdo->prepare("SELECT * FROM alarm WHERE user = ? ORDER BY alarm_time DESC");
        $statement->execute(array($id));
        $prevClient = "";
        while ($row = $statement->fetch()) {
            $i = $i + 1;

            if (strcmp($row['client'], $prevClient) != 0) {
                $i = 0;
                $prevClient = $row['client'];
                $statement2 = $pdo->prepare("SELECT name FROM client WHERE device_id = ?");
                $statement2->execute(array($row['client']));
                $name = $statement2->fetch();
            }
            if ($i < 10) {
                if ($row['alarmcode'] == 34) {
                    $alarm = $alarm . "\n" . $name['name'] . " - " . date('d.m.y H:i:s', strtotime($row['alarm_time'])) . " - Alarm";
                } elseif ($row['alarmcode'] == 51) {
                    $alarm = $alarm . "\n" . $name['name'] . " - " . date('d.m.y H:i:s', strtotime($row['alarm_time'])) . " - Anmeld.";
                }

            }
        }
        $content = array('chat_id' => $chat_id, 'text' => $alarm);
        $telegram->sendMessage($content);
    } elseif (strncmp($text, "/setLocation", 12) == 0) {
        // Build the reply array
        $client = strtok($text, '=');
        $client = strtok('=');

        $statement = $pdo->prepare("SELECT * FROM client WHERE name = :name");
        $result = $statement->execute(array('name' => $client));
        $user = $statement->fetch();
        if ($user) {
            $statement = $pdo->prepare("UPDATE client SET location = :location WHERE name = :name");
            $result = $statement->execute(array('location' => '0', 'name' => $client));
            $user = $statement->fetch();

            // Fake eintragung
            $option = [[
                $telegram->buildKeyboardButton('Mein Standort', $request_contact = false, $request_location = true),
            ],
            ];

            $keyb = $telegram->buildKeyboard($option, $onetime = true, $resize = true);
            $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'Bitte klicken Sie auf Mein Standort um diesen zu senden'];
            $telegram->sendMessage($content);
        } else {
            $content = array('chat_id' => $chat_id, 'text' => 'Kirrungsmelder nicht vorhanden!');
            $telegram->sendMessage($content);

        }

    } elseif ($text == '/kirrungen') {
        // Shows the Inline Keyboard and Trigger a callback on a button press
        $option = [
            [
                $telegram->buildInlineKeyBoardButton('Client', $url = '', $callback_data = '1'),
                $telegram->buildInlineKeyBoardButton('Basis', $url = '', $callback_data = '2'),
            ],
        ];

        $keyb = $telegram->buildInlineKeyBoard($option);
        $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'Bitte waehlen Sie Ihre Gruppe aus:'];
        $telegram->sendMessage($content);

    } elseif (strncmp($text, "/", 1) == 0) {
        $message = 'Kein gueltiger Befehl';
        $content = array('chat_id' => $chat_id, 'text' => $message);
        $telegram->sendMessage($content);
    }
}
# �berpr�fung, ob ein Standort geschickt wurde und dies eine Antwort auf eine Bot-Nachricht ist:
# --> Der gesendete Standort wird an die Stelle gespeichert die auf 0 zur�ckgesetzt wurde
if ($userLocation and !$reply) {
    $content = array('chat_id' => $chat_id, 'text' => $userLocation["longitude"] . "   " . $userLocation["latitude"]);
    $telegram->sendMessage($content);
} else if ($userLocation and $reply) {
    $statement = $pdo->prepare("SELECT * FROM client WHERE location = :location");
    $result = $statement->execute(array('location' => '0'));
    $user = $statement->fetch();
    if ($user) {
        $statement = $pdo->prepare("UPDATE client SET location = :location WHERE location = :location2");
        $result = $statement->execute(array('location' => $userLocation["longitude"] . "   " . $userLocation["latitude"], 'location2' => '0'));
        $user = $statement->fetch();
        $content = array('chat_id' => $chat_id, 'text' => 'Standort des Kirrungsmelders gesetzt!');
        $telegram->sendMessage($content);

    }
}
