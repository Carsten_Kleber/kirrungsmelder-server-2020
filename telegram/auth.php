<?php

# auth.php: Wird von telebot.php aufgerufen:
# �berpr�fung der Berechtigung des Telegram-Nutzers
# Die Telegram-ID wird mit den hinterlegten IDs verglichen
# Ist die Chat-ID nicht bekannt, wird das Skript abgebrochen
#   --> Der Zugang wird dem Nutzer verweigert

# Auslesen der Telegram-Daten:
$data = $telegram->getData();
# Suchen der hinterlegten Chat-ID in "users"
$statement = $pdo->prepare("SELECT * FROM users WHERE telegram_id = :chat_id");
$result = $statement->execute(array('chat_id' => $chat_id));
$auth = $statement->fetch();
$id = $auth['id'];

# Wenn nicht registriert:
if (!$auth) {

# �berpr�fung, ob Registrierungsanfrage
    if (strncmp($text, "/reg", 3) == 0) {
        $mail = strtok($text, '=');
        $mail = strtok('=');
        # Ist die angefragte Email bekannt und Telegram Online aktiviert?
        $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email AND telegram_active = 1");
        $result = $statement->execute(array('email' => $mail));
        $user = $statement->fetch();

# Falls nein --> Registrierung scheitert, wenn ja wird die Chat-ID gespeichert
        if (!$user) {
            $message = 'Registrierung gescheitert! Email falsch oder Telegram noch nicht auf https://www.kleber.dynu.net/telegram aktiviert.';
        } else {
            $message = 'Registrierung erfolgreich!';
            $statement = $pdo->prepare("UPDATE users SET telegram_id = :telegram_id WHERE email = :email");
            $result = $statement->execute(array('telegram_id' => $chat_id, 'email' => $mail));
            $user = $statement->fetch();

        }
    } else if (strncmp($text, "/", 1) == 0) {
        $message = 'Ihr Acount ist noch nicht registriert! Bitte registrieren sie sich mit /reg= gefolgt von Ihrem Aktivierungscode.';

    }
# R�ckgabe der Nachricht und Abbruch des Skripts, Anfrage beendet
    $content = array('chat_id' => $chat_id, 'text' => $message);
    $telegram->sendMessage($content);
    die();

} else {
# Skript l�uft weiter, wenn /unreg geschickt wird, wird die hinterlegte Telegram-ID gel�scht
    if (strcmp($text, "/unreg") == 0) {
        $content = array('chat_id' => $chat_id, 'text' => 'Kirr-Bot abgemeldet!');
        $statement = $pdo->prepare("UPDATE users SET telegram_id = :telegram_id WHERE id = :id");
        $result = $statement->execute(array('telegram_id' => "NULL", 'id' => $auth['id']));
        $user = $statement->fetch();
        $telegram->sendMessage($content);
        die();
    }
}

?>
