<?php

# telebot.php --> Hauptdatei der Telegram-Alarmierung
#   wird von Telegram bei einer neuen Nachricht kontaktiert
#   Datei verwendet die Telegram-Bibliothek: https://github.com/Eleirbag89/TelegramBotPHP
#   Datei inkludiert auth.php zur Authentifizierung des Nutzers
#   Datei inkludiert update.php zur Dekodierung der Telegram-Nachrichten

include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";
include 'Telegram.php';

# Telegram-Bot-Token aus Sicherheitsgr�nden ge�ndert
$bot_token = '990023456608:AAGAVZdfss45gR0x_4JSiIa5_Sm-szMTnY9o';
$telegram = new Telegram($bot_token);

# Auslesen der Telegram-Nachricht-Atrribute:
$text = $telegram->Text();
$name = $telegram->FirstName();
$userLocation = $telegram->Location();
$group = $telegram->messageFromGroup();
$chat_id = $telegram->ChatID();
$reply = $telegram->ReplyToMessageID();
$messageID = $telegram->MessageID();

# Authentifizierung des Nutzers:
include "auth.php";

# Dekodierung und Bearbeitung der Telegram-Nachrichten:
include "update.php";

?>




