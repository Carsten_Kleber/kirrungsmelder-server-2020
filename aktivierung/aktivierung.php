<?php

# Über den Reiter Aktivierung können Basisstationen zum Nutzer hinzugefügt werden
#   Benötigt werden:
#       Geräte-ID
#       Aktivierungscode (md5-Hash der Geräte-ID)
#       Name des Gerätes
# Erreichen der Seite nur nach Anmeldung möglich

include $_SERVER['DOCUMENT_ROOT'] . "/checkPermission.php";
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";?>

<h1> Aktivierung einer Basisstation: </h1>
<!DOCTYPE html>
<html>

<head>
</head>

<body>
    <?php
$showFormular = true; //Variable ob das Aktivierungsformular angezeigt werden soll

#Wenn das Formular abgeschickt wird, werden die Parameter als POST übermittelt und hier bearbeitet:
if (isset($_GET['activate'])) {
    $error = false;
    $id = $_POST['id'];
    $name = $_POST['name'];
    $code = $_POST['code'];

    # Bei einem Fehler wird eine oder mehrere rote Fehler ausgegeben:
    if (strlen($id) == 0) { # Überprüfung id Länge >0
    echo '<font color="#FF0000">Bitte eine gültige Basis-ID angeben</font><br>';
        $error = true;
    }
    if (strlen($code) == 0) { # Überprüfung, ob ein Aktivierungsschlüssel angegeben wurde
    echo '<font color="#FF0000">Bitte einen gültigen Aktivierungsschlüssel angeben</font><br>';
        $error = true;
    }
    if ($code != md5($id) && !$error) { # Überprüfung, ob der Aktivierungsschlüssel richtig ist
    echo '<font color="#FF0000">Der Aktivierungscode ist ungültig. Bitte versuchen Sie es erneut.</font><br>';
        $error = true;
    }

    //Überprüfe, dass die Basisstation noch nicht registriert wurde
    if (!$error) {
        $statement = $pdo->prepare("SELECT * FROM basis WHERE device_id = :device_id");
        $result = $statement->execute(array('device_id' => $id));
        $basis = $statement->fetch();

        if ($basis !== false) {
            echo '<font color="#FF0000">Diese Basisstation ist bereits registriert</font><br>';
            $error = true;
        }
    }
    //Keine Fehler, wir können das Gerät registrieren
    if (!$error) {

        #Einfügen des Gerätes in die Datenbanktabelle Basis:

        $statement = $pdo->prepare("INSERT INTO basis (device_id, user, name) VALUES (:device_id, :user, :name)");
        $result = $statement->execute(array('device_id' => $id, 'user' => $_SESSION['userid'], 'name' => $name));

        if ($result) { # Darstellen eines Infotextes und Entfernen des HTML-Formulars
        echo 'Ihre Basisstation wurde erfolgreich registriert. <a href="https://www.kleber.dynu.net">Zur Startseite</a>';
            $showFormular = false;
        } else {
            echo '<font color="#FF0000">Beim Abspeichern ist leider ein Fehler aufgetreten</font><br>';
        }
    }
}

# Darstellen des HTML-Formulars:
if ($showFormular) {
    ?>
        <form action="?activate=1" method="post">
            <br>Geräte-ID:<br>
            <input type="text" size="40" maxlength="250" name="id"><br><br> Name (opt.):<br>
            <input type="text" size="40" maxlength="250" name="name"><br><br> Aktivierungscode:
            <br>
            <input type="text" size="40" maxlength="250" name="code"><br><br>
            <input type="submit" value="Abschicken"><br><br>
        </form>
        <?php
}
; //Ende von if($showFormular)
?>
</body>

</html>
