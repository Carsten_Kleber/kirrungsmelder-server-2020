<?php
# Datei zum Generieren eines Test-Aktivierungsschlüssel zur Geräteaktivierung
# Die ID wird über einen GET-Befehl übergeben und mit der
# md5-Funktion gehashed
# Ausgabe auf einer blanko-Seite

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $pw_hash = md5($id);

    echo $pw_hash;
}
?>
