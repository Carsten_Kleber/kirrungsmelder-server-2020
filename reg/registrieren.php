<?php

# registrieren.php: Registrierung eines Nutzers über ein Registrierungsformular
# Die Email-Adresse darf noch nicht registriert sein
# Registrierung basiert auf: https://www.php-einfach.de/experte/php-codebeispiele/loginscript/

session_start();
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";
if (isset($_SESSION['userid'])) {
    die('Sie sind bereits eingeloggt!');
}
# Wenn die Registrierung in der head.php deaktiviert ist, wird das Formular nicht angezeigt
if (!$reg_enable) {die('Keine Registrierung möglich! Bitte versuchen Sie es zu einem späteren Zeitpunkt erneut.');}
$userid = $_SESSION['userid'];

?>
<h1> Registrierung: </h1>
<!DOCTYPE html>
<html>
<head>
  <title>Registrierung</title>
</head>
<body>

<?php
$showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll

# Nach dem Abschicken des Formular wird GET['register'] gesetzt
if (isset($_GET['register'])) {
    $error = false;
    $email = $_POST['email'];
    $vorname = $_POST['vorname'];
    $nachname = $_POST['nachname'];
    $passwort = $_POST['passwort'];
    $passwort2 = $_POST['passwort2'];

    # Überprüfung der Email-Adresse (enthält @ etc)
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo '<font color="#FF0000">Bitte eine gültige E-Mail-Adresse eingeben</font><br>';
        $error = true;
    }
    # Überprüfung des Passwortes
    if (strlen($passwort) == 0) {
        echo '<font color="#FF0000">Bitte ein Passwort angeben</font><br>';
        $error = true;
    }
    # Überprüfung der zweiten Eingabe des Passwortes
    if ($passwort != $passwort2) {
        echo '<font color="#FF0000">Die Passwörter müssen übereinstimmen</font><br>';
        $error = true;
    }

    //Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
    if (!$error) {
        $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
        $result = $statement->execute(array('email' => $email));
        $user = $statement->fetch();

        if ($user !== false) {
            echo '<font color="#FF0000">Diese E-Mail-Adresse ist bereits vergeben</font><br>';
            $error = true;
        }
    }
    //Keine Fehler, wir können den Nutzer registrieren
    if (!$error) {
        $passwort_hash = password_hash($passwort, PASSWORD_DEFAULT);

        # Anlegen einer neuen Zeile in der Datenbank "user" mit Email und Passwort-Hash
        $statement = $pdo->prepare("INSERT INTO users (email, passwort, vorname, nachname) VALUES (:email, :passwort, :vorname, :nachname)");
        $result = $statement->execute(array('email' => $email, 'passwort' => $passwort_hash, 'vorname' => $vorname, 'nachname' => $nachname));

        if ($result) {
            echo 'Sie wurden erfolgreich registriert. <a href="https://www.kleber.dynu.net/login">Zum Login</a>';
            $showFormular = false;
        } else {
            echo '<font color="#FF0000">Beim Abspeichern ist leider ein Fehler aufgetreten</font><br>';
        }
    }
}

if ($showFormular) {
    ?>
<!-- Registrierungsformular: Email, Vorname, Nachname, Passwort(2x) -->
<form action="?register=1" method="post">
<br>E-Mail:<br>
<input type="email" size="40" maxlength="250" name="email"><br><br>
Vorname:<br>
<input type="text" size="40" maxlength="250" name="vorname"><br><br>
Nachname:<br>
<input type="text" size="40" maxlength="250" name="nachname"><br><br>
Dein Passwort:<br>
<input type="password" size="40"  maxlength="250" name="passwort"><br>

Passwort wiederholen:<br>
<input type="password" size="40" maxlength="250" name="passwort2"><br><br>

<input type="submit" value="Abschicken"><br><br>
<p> Bereits registriert? <a href="https://www.kleber.dynu.net/login">Hier anmelden.</a></p><br>
</form>

<?php
}
; //Ende von if($showFormular)
?>

</body>
</html>

