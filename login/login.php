<?php

# login.php: Authorisierung des Nutzers durch einen Login-Berreich
# Nutzung der PHP-Funktion "Session" --> Automatische Vergabe einer Session-ID
# Login-Skript basiert auf: https://www.php-einfach.de/experte/php-codebeispiele/loginscript/
# Seite ist ohne Anmeldung erreichbar,

include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";

# Erkennung, ob der Nutzer bereits eingeloggt ist
if (isset($_SESSION['userid'])) {
    die('Sie sind bereits eingeloggt!');
}

# Nach Abschicken des Login-Formular existiert GET['login']
if (isset($_GET['login'])) {
    $email = $_POST['email'];
    $passwort = $_POST['passwort'];

    # Auslesen der Email und des Passwortes aus der Datenbank "user":
    $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
    $result = $statement->execute(array('email' => $email));
    $user = $statement->fetch();

    //Überprüfung des Passworts
    if ($user !== false && password_verify($passwort, $user['passwort'])) {
        $_SESSION['userid'] = $user['id'];
        # Weiterleitung zur angeforderten Seite nach 1s
        echo '<meta http-equiv="refresh" content="1; URL=https://www.kleber.dynu.net/' . $_GET['site'] . '">';
        die('Login erfolgreich. Sie werden weitergeleitet.');
    }
    # Fehlermeldung wenn ein Fehler auftritt
    else {
        $errorMessage = "E-Mail oder Passwort war ungültig<br>";
    }

}

?>
    <h1> Login: </h1>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Login</title>
    </head>

    <body>
        <?php
# Darstellung der Fehlermeldung in Rot:

if (isset($errorMessage)) {
    echo '<font color="#FF0000">';
    echo $errorMessage;
    echo '</font>';
}
?>
            <?php
echo '<form action="?login=1&site=' . $_GET['site'] . '" method="post">';
?>
            <!-- Anmeldungs-Formular -->
            <br>E-Mail:<br>
            <input type="email" size="40" maxlength="250" name="email"><br><br> Dein Passwort:<br>
            <input type="password" size="40" maxlength="250" name="passwort"><br><br>
            <input type="submit" value="Abschicken"><br><br>
            <p> Noch nicht registriert? <a href="https://www.kleber.dynu.net/reg">Hier registrieren.</a></p><br>
            </form>
    </body>

    </html>
