<?php

# Darstellung aller Geräte mit Kenndaten
# Auswahl zwischen Client, Basis und Karte
# Die Seite kann nur nach einer Anmeldung erreicht werden.

include $_SERVER['DOCUMENT_ROOT'] . "/checkPermission.php";
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";
$battMax = 3000; # Maximale Batteriespannung in mV

?>
    <article>
        <h1>Aktuelle Geräte: </h1>
        <p align=center>
            <!-- Auswahl der Kategorie -->
            <a href=index.php?type=client>Client</a><br>
            <a href=index.php?type=basis>Basis</a><br>
            <a href=index.php?type=map>Karte</a>
        </p>
        <?php

$i = 0;
# Abfrage, welche Basisstationen dem Nutzer gehören:
$statement = $pdo->prepare("SELECT * FROM basis WHERE user = ?");
$statement->execute(array($_SESSION['userid']));
while ($row = $statement->fetch()) {
    $device[$i] = $row['device_id'];
    $i = $i + 1;
    # Nach der Schleife enthält i die Anzahl an Basisstationen
    #device die IDs der Stationen
}
# Auslesen des Vornamens des Nutzers
$statement3 = $pdo->prepare("SELECT * FROM users WHERE id = ?");
$statement3->execute(array($_SESSION['userid']));
$row3 = $statement3->fetch();
$vorname = $row3['vorname'];

# Dekodieren der Kategoriewahl des Users, wird über GET-Parameter übetragen
if (isset($_GET['type'])) {
    $type = $_GET['type'];
    if ($type == "basis") {?>
            <h2>Basis </h2>
            <table border="1" width="900" cellspacing="0">
                <?php
        # Auslesen der Basisstationen und Darstellung in HTML-Tabelle:
        $statement = $pdo->prepare("SELECT * FROM basis WHERE user = ?");
        $statement->execute(array($_SESSION['userid']));
        ?>
                    <thead>
                        <tr>
                            <th>ID</td>
                                <th>Nutzer</td>
                                    <th>Name</td>
                                        <th>Standort</td>
                                            <th>Batterie</td>
                                                <th>GSM Empfang</td>
                                                    <th>Guthaben</td>
                                                        <th>Letzte Aktivitaet</td>
                                                            <th>Bearbeiten</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
# Eingabe der Daten in die Tabelle der Basisstationen und Umsetzen in lesbarers Format
        while ($row = $statement->fetch()) {
            $device[$i] = $row['device_id'];
            echo "<tr>";
            echo "<td>" . $row['device_id'] . "</td>";
            echo "<td>" . $vorname . "</td>";
            echo "<td>" . $row['name'] . "</td>";
            echo "<td>" . $row['location'] . "</td>";
            echo "<td>" . $row['battery_voltage'] . "</td>";
            if ($row['gsm_qos'] < 10) {
                echo "<td> Schlecht </td>";
            } else if ($row['gsm_qos'] < 15) {
                echo "<td> OK </td>";
            } else if ($row['gsm_qos'] < 20) {
                echo "<td> Gut </td>";
            } else if ($row['gsm_qos'] < 30) {
                echo "<td> Excellent </td>";
            } else {
                echo "<td> ERROR </td>";
            }

            echo "<td>" . $row['credit'] . "</td>";
            echo "<td>" . date('d.m.y H:i:s', strtotime($row['last_message'])) . "</td>";
            echo '<td><form action="https://www.kleber.dynu.net/geraete/index.php" method="get"><button type="submit" name="changeBasis" value="' . $row['device_id'] . '">X</button></td>';
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";

    }
    # Darstellung der Clients, Analog wie bei den Basisstationen:
    else if ($type == "client") {?>
                            <h2>Client </h2>
                            <table border="1" width="900" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</td>
                                            <th>Zug. Basis</td>
                                                <th>Name</td>
                                                    <th>Standort</td>
                                                        <th>Batterie</td>
                                                            <th>Lora Empfang</td>
                                                                <th>Letzter Alarm</td>
                                                                    <th>Bearbeiten</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
# Jede Basisstation wird überprüft und die zugehörigen Clients ausgelesen
    # für i Basistationen muss die Tabelle clients ausgelesen werden:

        for ($k = 0; $k < $i; $k++) {
            $statement2 = $pdo->prepare("SELECT * FROM client WHERE device_basis = ?");
            $statement2->execute(array($device[$k]));

            while ($row2 = $statement2->fetch()) {
                echo "<tr>";
                echo "<td>" . $row2['device_id'] . "</td>";
                echo "<td>" . $row2['device_basis'] . "</td>";
                echo "<td>" . $row2['name'] . "</td>";
                echo "<td>" . $row2['location'] . "</td>";
                echo "<td>" . round(($row2['battery_voltage'] / $battMax) * 100, 0) . " %</td>";
                if ($row2['lora_qos'] < -120) {
                    echo "<td> Schlecht </td>";
                } else if ($row2['lora_qos'] < -90) {
                    echo "<td> OK </td>";
                } else if ($row2['lora_qos'] < -60) {
                    echo "<td> Gut </td>";
                } else if ($row2['lora_qos'] < -30) {
                    echo "<td> Excellent </td>";
                } else {
                    echo "<td> ERROR </td>";
                }

                echo "<td>" . date('d.m.y H:i:s', strtotime($row2['last_alarm'])) . "</td>";
                echo '<td><form action="https://www.kleber.dynu.net/geraete/index.php" method="get"><button type="submit" name="changeClient" value="' . $row2['device_id'] . '">X</button></td>';
                echo "</tr>";

            }
        }
        echo "</tbody>";
        echo "</table>";

    } else if ($type == "map") {
        include "map.htm";
    }
    # Wenn die Karte ausgewählt wird, wird map.htm eingebunden

    echo "<br /> <br /> ";

}
?>
    </article>