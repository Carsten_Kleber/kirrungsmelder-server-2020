<?php

# Änderungen an Geräten werden in dieser Datei bearbeitet
# Der Name kann geändert werden, Geräte können gelöscht werden
# Die Seite kann nur nach einer Anmeldung erreicht werden.

include $_SERVER['DOCUMENT_ROOT'] . "/checkPermission.php";
include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";

?>

<article>
    <h1>Bearbeitung der Geräte: </h1>

 <?php
if (!isset($_GET['submit'])) {
    $showFormular = true;
    # Änderung von Basisstationen, Übergabe der Geräte-ID über GET-Parameter
    if (isset($_GET['changeBasis'])) {
        $statement = $pdo->prepare("SELECT name FROM basis WHERE user = :user and device_id = :device_id ");
        $statement->execute(array('user' => $_SESSION['userid'], 'device_id' => $_GET['changeBasis']));
        $row = $statement->fetch();
        if ($row) {
            echo '<p align=center>Aktueller Name der Basisstation Nr. ' . $_GET['changeBasis'] . ': </p> <p align=center> <b>' . $row['name'] . '</b></p>';
        }

    }
    # Änderung von Kirrungsmelder, Übergabe der Geräte-ID über GET-Parameter
    elseif (isset($_GET['changeClient'])) {
        $statement = $pdo->prepare("SELECT name FROM client WHERE device_id = :device_id ");
        $statement->execute(array('device_id' => $_GET['changeClient']));
        $row = $statement->fetch();
        if ($row) {
            echo '<p align=center>Aktueller Name des Clients Nr. ' . $_GET['changeClient'] . ': </p> <p align=center> <b>' . $row['name'] . '</b></p>';
        }

    }
} else if (isset($_POST['type'])) {

    # Aufruf wenn einer neuer Name per POST übermittelt wurde

    $no = substr($_POST['type'], 1);
    $name = $_POST['name'];

    if (strncmp($_POST['type'], "c", 1) == 0) {
        $statement = $pdo->prepare("UPDATE client SET name = :name WHERE device_id = :device_id ");
    } else {
        $statement = $pdo->prepare("UPDATE basis SET name = :name WHERE device_id = :device_id ");
    }
    $statement->execute(array('name' => $name, 'device_id' => $no));
    $row = $statement->fetchAll();
    $updateCount = $statement->rowCount();
    if ($updateCount) {
        echo "Ihre Aenderungen wurden erfolgreich gespeichert.<br><br>";
    } else {
        echo "Ein Fehler ist aufgetreten.<br><br>";
    }

    $showFormular = false;
} else if (isset($_POST['delete'])) {

# Aufruf, wenn das Gerät gelöscht werden soll

    $no = substr($_POST['delete'], 1);

    if (strncmp($_POST['delete'], "c", 1) == 0) {
        $statement = $pdo->prepare("DELETE FROM client WHERE device_id = :device_id ");
    } else {
        $statement = $pdo->prepare("DELETE FROM basis WHERE device_id = :device_id ");
    }
    $statement->execute(array('device_id' => $no));
    $row = $statement->fetchAll();

    $updateCount = $statement->rowCount();
    if ($updateCount) {
        echo "Das Gerät wurde erfolgreich gel�scht.<br><br>";
    } else {
        echo "Ein Fehler ist aufgetreten.<br><br>";
    }

    $showFormular = false;
}

# Formular zur Eingabe des neuen Namens
if ($showFormular) {
    ?>
        <form action="?submit=1" method="post">
            <center>
                <br>Neuer Name:<br>
                <?php
echo '<input type="text" size="40" maxlength="250" name="name" value ="' . $row['name'] . '"><br><br>';
    if (isset($_GET['changeClient'])) {
        echo '<button type="submit" name="type" value="c' . $_GET['changeClient'] . '">Abschicken</button><br><br>';
        echo '<button type="submit" name="delete" value="c' . $_GET['changeClient'] . '">!! Client loeschen !!</button><br><br>';
    } else if (isset($_GET['changeBasis'])) {
        echo '<button type="submit" name="type" value="b' . $_GET['changeBasis'] . '">Abschicken</button><br><br>';
        echo '<button type="submit" name="delete" value="b' . $_GET['changeBasis'] . '">!! Basis loeschen !!</button><br><br>';
    }
    ?>
        </form>
        </center>
        <br><br>
        <?php

}
?>
        <li><a href=https://www.kleber.dynu.net/geraete/index.php?type=client>Client</a></li>
        <li><a href=https://www.kleber.dynu.net/geraete/index.php?type=basis>Basis</a></li>
