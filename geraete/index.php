 <?php

# Index.php des Unterordners Ger�te
# Im Normalfall wird die geraete.php ge�ffnet
# Bei Auswahl eines Bearbeitungsbuttons wird change.php inkludiert
#   --> Bearbeitung der Kenndaten
# Sonstiger Aufbau wie Standard-Index.php

include $_SERVER['DOCUMENT_ROOT'] . "/head.php";

if (isset($_GET['changeClient']) or isset($_GET['changeBasis']) or isset($_GET['submit'])) {
    include "change.php";
} else {
    include "geraete.php";
}

include $_SERVER['DOCUMENT_ROOT'] . "/bottom.php";
?>



