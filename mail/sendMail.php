<?php

# sendMail: Regelt das Senden von Alarmen per Mail.
# Genutzt wird der SMTP-Server von Web.de mit einem privaten Mail-Account
# Die Nutzer-Email wird der aus Tabelle "users" ausgelesen

include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";

#Auslesen der Email-Adresse
$statement = $pdo->prepare("SELECT email FROM users WHERE id = ?");
$statement->execute(array($Nutzer));
$user = $statement->fetch();

# Account-Daten des Web.de Accounts, Daten aus Sicherheitsgründen verändert
$to = $user['email'];
$from = 'test_test@web.de';
$client = $client['name'];
$basis = $user2['name'];

# Inkludierung der PHPMailer-Bibliothek
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

#PHPMailer-Besipiel basiert auf: https://github.com/PHPMailer/PHPMailer

require 'PHPMailer.php';
require 'SMTP.php';
require 'Exception.php';
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = 0; // Enable verbose debug output
    $mail->isSMTP(); // Send using SMTP
    $mail->Host = 'smtp.web.de'; // Set the SMTP server to send through
    $mail->SMTPAuth = true; // Enable SMTP authentication
    $mail->Username = 'test_test@web.de'; // SMTP username
    $mail->Password = 'password'; // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port = 587; // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom($from, 'Kirrserver');
    $mail->addAddress($to);

    // Content
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = 'Aktivität an Kirrung';
    $mail->Body = 'Der Kirrungsmelder ' . $basis . ' meldet eine Aktivität am Client ' . $client;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
?>
