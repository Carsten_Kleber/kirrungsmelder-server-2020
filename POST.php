<?php

# Verwaltung der Schnittstelle zum GPRS-Modul
# Dekodieren und Speichern der Nachrichten der Basisstationen
# Triggern von Alarmen per Telegram und per Mail

include $_SERVER['DOCUMENT_ROOT'] . "/dblogin.php";
include $_SERVER['DOCUMENT_ROOT'] . "/telegram/Telegram.php"; #Telegram-Bibliothek: https://github.com/Eleirbag89/TelegramBotPHP

# Bot-Token aus Sicherheitsgr�nden ge�ndert:
$bot_token = '43455348:AAGAVZazw2BoR0x_4JSiIa5_Sm-szMTnY9o';
$telegram = new Telegram($bot_token);

if ($_POST["auth"] == "sfsrw3e") { #Authentifizierungscode aus Sicherheitsgr�nden ge�ndert

    $timestamp = date("d.m.Y - H:i:s");
    $alarmcode = $_POST["alarmcode"];
    $basisid = $_POST["basisid"];
    $clientid = $_POST["clientid"];
    $clientBatt = $_POST["clientBatt"];
    $loraQos = $_POST["loraQos"];
    $basisBatt = $_POST["basisBatt"];
    $gsmQos = $_POST["gsmQos"];
    $credit = $_POST["credit"];

    # �berpr�fung, ob die Basisstation bereits aktiviert ist:
    $statement = $pdo->prepare("SELECT * FROM basis WHERE device_id = :device_id");
    $result = $statement->execute(array('device_id' => $basisid));
    $basis_data = $statement->fetch();
    $Nutzer = $basis_data['user'];

    #Ist dies nicht der Fall, werden die Daten verworfen
    if ($basis_data == false) {
        die();
    }
    # Bei einer aktivierten Basisstation wird die Datenbank aktualisiert:
    $statement = $pdo->prepare("UPDATE basis SET battery_voltage = :battery_voltage, gsm_qos = :gsm_qos, credit = :credit WHERE device_id = :device_id");
    $result = $statement->execute(array('battery_voltage' => $basisBatt, 'gsm_qos' => $gsmQos, 'credit' => $credit, 'device_id' => $basisid));
    $user = $statement->fetch();

    # �berpr�fung, ob der Kirrungsmelder (Client) bereits in der Tabelle client eingetragen ist:
    $statement = $pdo->prepare("SELECT * FROM client WHERE device_id = :device_id");
    $result = $statement->execute(array('device_id' => $clientid));
    $client = $statement->fetch();

    # Wenn nicht: Erstellen eines neuen Eintrages, ansonsten: Aktualisieren des vorhandenen Eintrages
    if ($client == false) {
        $statement = $pdo->prepare("INSERT INTO client (device_id, device_basis, battery_voltage,lora_qos) VALUES (:device_id, :device_basis, :battery_voltage,:lora_qos)");
        $result = $statement->execute(array('device_id' => $clientid, 'device_basis' => $basisid, 'battery_voltage' => $clientBatt, 'lora_qos' => $loraQos));
        $user = $statement->fetch();
    } else {
        $statement = $pdo->prepare("UPDATE client SET device_basis =:device_basis, battery_voltage =:battery_voltage,lora_qos =:lora_qos WHERE device_id = :device_id");
        $result = $statement->execute(array('device_basis' => $basisid, 'battery_voltage' => $clientBatt, 'lora_qos' => $loraQos, 'device_id' => $clientid));
        $user = $statement->fetch();

    }

    # Zus�tzlich wird der Alarm in der Tabelle alarm gespeichert
    $statement = $pdo->prepare("INSERT INTO alarm (client, user, alarmcode) VALUES (:client, :user, :alarmcode)");
    $result = $statement->execute(array('client' => $clientid, 'user' => $Nutzer, 'alarmcode' => $alarmcode));
    $user = $statement->fetch();

    # Auslesen der Benachrichtigungseinstellungen des Nutzers (Telegram und Email):
    $statement = $pdo->prepare("SELECT telegram_id, telegram_active, mail_active FROM users WHERE id = :id");
    $result = $statement->execute(array('id' => $Nutzer));
    $auth = $statement->fetch();

    # Je nach Einstellungen und vorhandenen Daten der Ger�te werden unterschiedliche Nachrichten erzeugt..

    if ($auth['telegram_id'] and $auth['telegram_active']) {
        if ($basis_data['name'] and $client['name']) {
            $message = 'Alarm von Basistation ' . $basis_data['name'] . ', Client ' . $client['name'];
        } else if ($basis_data['name']) {
            $message = 'Alarm von Basistation ' . $basis_data['name'] . ', Client ' . $clientid;
        } else if ($client['name']) {
            $message = 'Alarm von Basistation ' . $basisid . ', Client ' . $client['name'];
        } else {
            $message = 'Alarm von Basistation ' . $basisid . ', Client ' . $clientid;
        }
        # ... und hier gesendet.
        $content = array('chat_id' => $auth['telegram_id'], 'text' => $message);
        $telegram->sendMessage($content);
    }
    # Ist die Benachrichtigung per Email aktiviert, wird die Datei "sendMail.php" eingebunden.
    if ($auth['mail_active']) {
        include $_SERVER['DOCUMENT_ROOT'] . "/mail/sendMail.php";
    }

}
?>


